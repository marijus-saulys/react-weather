import axios from 'axios';
import _ from 'lodash';

import { api, maxFavorites } from '../../../config';
import handleError from './errorHandler';

const saveFavorite = (country, city) => {
  let favorites = localStorage.getItem('favorite');
  favorites = favorites ? JSON.parse(favorites) : [];
  favorites = _.filter(favorites, item => !((item.country === country) && (item.city === city)));
  if (favorites.length === maxFavorites) {
    favorites.splice(0, 1);
  }
  favorites.push({ country, city });
  localStorage.setItem('favorite', JSON.stringify(favorites));
};

const action = (country, city, save = true) => async () => {
  try {
    const { data } = await axios({
      url: `${api.weather.API_ROOT}?q=${city},${country}&units=metric&APPID=${api.weather.API_KEY}`,
      method: 'GET',
    });
    if (save) {
      saveFavorite(country, city);
    }
    return data;
  } catch (error) {
    throw new Error(handleError(error));
  }
};

export default action;

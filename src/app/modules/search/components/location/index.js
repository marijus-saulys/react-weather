import Manual, { EVENTS as MANUAL_EVENTS } from './manual';
import Current, { EVENTS as CURRENT_EVENTS }  from './current';
import Saved, { EVENTS as SAVED_EVENTS }  from './saved';

export const EVENTS = {
  ...MANUAL_EVENTS,
  ...CURRENT_EVENTS,
  ...SAVED_EVENTS,
};

export default {
  Manual,
  Current,
  Saved,
};

import React from 'react';

import View, { EVENTS } from './view';
import { ACTION, API } from './actions';
import { locations } from '../../config';
import Emitter from '../../utils/Emitter';

class Component extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      action: ACTION.DONE,
      data: null,
      error: false,
      selectedCountry: locations.default.country,
      selectedCity: locations.default.city,
    };

    this.props.subscribe({
      [EVENTS.COUNTRY_CHANGED]: this.handleCountryChanged,
      [EVENTS.CITY_CHANGED]: this.handleCityChanged,
      [EVENTS.SEARCH]: this.handleSearch,
      [EVENTS.SHOW_CURRENT_LOCATION]: this.handleShowCurrentLocation,
    });
  }

  handleCountryChanged = ({ target }) => {
    const defaultCity = Object.keys(locations.countries[target.value].cities)[0];
    this.setState({
      selectedCountry: target.value,
      selectedCity: locations.countries[target.value].cities[defaultCity],
    });
  };

  handleCityChanged = ({ target }) => {
    this.setState({ selectedCity: target.value });
  };

  handleSearch = async (e, { country = this.state.selectedCountry, city = this.state.selectedCity, save = true }) => {
    e.preventDefault();
    await this.callAPI(API.getByCity(country, city, save));
  };

  handleShowCurrentLocation = async (e, coords) => {
    if (!coords) {
      this.setState({
        data: null,
        error: { message: 'Could not detect your current location' },
      });
      return;
    }
    await this.callAPI(API.getByCoords(coords.latitude, coords.longitude));
  };

  callAPI = async (api) => {
    try {
      this.setState({
        action: ACTION.FETCHING,
        error: false,
      });
      const data = await api();
      this.setState({
        action: ACTION.DONE,
        data,
      });
    } catch (error) {
      this.setState({
        action: ACTION.DONE,
        data: null,
        error,
      });
    }
  };

  render() {
    return (
      <View {...this.state} {...this.props} loading={this.state.action !== ACTION.DONE}/>
    );
  }
}

export default Emitter(Component);

import React from 'react';

import { locations } from '../../config';
import Locations, { EVENTS as LOCATION_EVENTS } from './components/location';
import Map from './components/map';

export const EVENTS = {
  ...LOCATION_EVENTS,
};

const view = ({ data, error, ...props }) => (
  <div className="wrapper">
    <div className="container">
      <Locations.Manual {...props}/>
      <Locations.Current emit={props.emit} loading={props.loading}/>
      <Locations.Saved {...props}/>
    </div>
    {
      (data) ? (
        <div>
          <div className="container">
            <span>{locations.countries[data.sys.country] ? locations.countries[data.sys.country].name : '-'},</span>
            {
              locations.countries[data.sys.country] &&
              <>
                <span>
                  {
                    locations.countries[data.sys.country].cities[data.name]
                      ? locations.countries[data.sys.country].cities[data.name] : '-'
                  }
                </span>
              </>
            }
          </div>
          <div className="container">
            <div><span>Humidity:</span><span>{data.main.humidity}%</span></div>
            <div><span>Pressure:</span><span>{data.main.pressure} hPa</span></div>
            <div><span>Temperature:</span><span>{data.main.temp}°</span></div>
          </div>
          {!props.loading && <Map data={data}/>}
        </div>
      ) : (error && <div className="msg-danger">Error: {error.message}</div>)
    }
  </div>
);

export default view;

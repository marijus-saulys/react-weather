import React from 'react';
import _ from 'lodash';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { locations } from '../../../../config';

export const EVENTS = {
  COUNTRY_CHANGED: 'COUNTRY_CHANGED',
  CITY_CHANGED: 'CITY_CHANGED',
  SEARCH: 'SEARCH',
};

const manual = ({ selectedCountry, selectedCity, loading, emit }) => (
  <>
    <span>
      <select name="country" value={selectedCountry} onChange={emit(EVENTS.COUNTRY_CHANGED)}>
        {
          _.map(locations.countries, (country, key) => (
            <option key={key} value={key}>{country.name}</option>
          ))
        }
      </select>
    </span>
    <span>
      <select name="city" value={selectedCity} onChange={emit(EVENTS.CITY_CHANGED)}>
        {
          _.map(locations.countries[selectedCountry].cities, (city, key) => (
            <option key={key} value={key}>{city}</option>
          ))
        }
      </select>
    </span>
    {!!selectedCountry && !!selectedCity && (
      <span>
        <button disabled={loading} onClick={emit(EVENTS.SEARCH, {})}>
          <FontAwesomeIcon icon={loading ? 'spinner' : 'search-location'} spin={loading}/>
        </button>
      </span>
    )}
  </>
);

export default manual;

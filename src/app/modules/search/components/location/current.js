import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { geolocated } from 'react-geolocated';

export const EVENTS = {
  SHOW_CURRENT_LOCATION: 'SHOW_CURRENT_LOCATION',
};

const current = ({ emit, loading, ...props }) => props.isGeolocationAvailable && (
  <span>
    <button onClick={emit(EVENTS.SHOW_CURRENT_LOCATION, props.coords)}>
      <FontAwesomeIcon icon={loading ? 'spinner' : 'location-arrow'} spin={loading}/>
    </button>
  </span>
);

export default geolocated({
  positionOptions: { enableHighAccuracy: false },
  userDecisionTimeout: 5000,
})(current);

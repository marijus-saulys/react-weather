import getByCity from './getByCity';
import getByCoords from './getByCoords';

const ACTION = {
  FETCHING: 'FETCHING',
  DONE: 'DONE',
};

const API = {
  getByCity,
  getByCoords,
};

export {
  ACTION,
  API,
};

## Weather app

`npm run dev` - start project in local development environment.

`npm run build` - prepare project for deployment to production.

app configuration: `./src/app/config`

environment vars: `env.local`

Putting env.local to git with personal API keys for simplicity. Normally it would be in .gitignore and everyone using the project would need to setup own keys.

Keys will be valid until 2018.11.01. Use your own keys after that.

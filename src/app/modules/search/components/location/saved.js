import React from 'react';
import _ from 'lodash';

import { locations } from '../../../../config';

export const EVENTS = {
  SEARCH: 'SEARCH',
};

const saved = ({ emit }) => {
  let favorites = localStorage.getItem('favorite');
  favorites = favorites ? JSON.parse(favorites) : [];
  return (
    <div>
      <ul>
      {
        _.map(favorites, (item, idx) => (
          <li key={idx}>
            <a href='/' onClick={emit(EVENTS.SEARCH, { ...item, save: false })}>
              {locations.countries[item.country].name}, {locations.countries[item.country].cities[item.city]}
            </a>
          </li>
        ))
      }
      </ul>
    </div>
  );
};

export default saved;

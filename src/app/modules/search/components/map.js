import React from 'react';
import GoogleMapReact from 'google-map-react';

import { api } from '../../../config';

const Message = ({ data: { main: weather } }) => (
  <div className="popup">
    <div><span>Humidity:</span><span>{weather.humidity}%</span></div>
    <div><span>Pressure:</span><span>{weather.pressure}hPa</span></div>
    <div><span>Temperature:</span><span>{weather.temp}°</span></div>
  </div>
);

const map = ({ data }) => (
  <div className="container" style={{ height: '50vh', width: '100%' }}>
    <GoogleMapReact
      bootstrapURLKeys={{ key: api.maps.API_KEY }}
      defaultCenter={{lat: data.coord.lat, lng: data.coord.lon}}
      defaultZoom={10}
    >
      <Message lat={data.coord.lat} lng={data.coord.lon} data={data}/>
    </GoogleMapReact>
  </div>
);

export default map;

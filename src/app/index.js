import React from 'react';

import './index.css';
import Search from './modules/search';

const Component = () => (
  <div className="app">
    <h1>Weather App</h1>
    <Search/>
  </div>
);

export default Component;

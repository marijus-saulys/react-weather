const errorHandler = (error) =>
  (error.response) ? error.response.data.message : `Error occurred: ${error.message}. Contact administrator.`;

export default errorHandler;

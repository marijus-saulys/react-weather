const api = {
  weather: {
    API_KEY: process.env.REACT_APP_WEATHER_API_KEY,
    API_ROOT: 'http://api.openweathermap.org/data/2.5/weather'
  },
  maps: {
    API_KEY: process.env.REACT_APP_MAPS_API_KEY,
  }
};

const maxFavorites = 5;

const locations = {
  default: {
    country: 'LT',
    city: 'Klaipeda',
  },
  countries: {
    'LT': {
      name: 'Lithuania',
      cities: {
        'Vilnius': 'Vilnius',
        'Kaunas': 'Kaunas',
        'Klaipeda': 'Klaipėda',
        'Telsiai': 'Telšiai',
        'Kazlu ruda': 'Kazlų rūda',
        'Sirvintos': 'Širvintos',
      }
    },
    'LV': {
      name: 'Latvia',
      cities: {
        'Riga': 'Riga',
        'Ventspils': 'Ventspils',
        'Jurmala': 'Jūrmala',
        'Ragaciems': 'Ragaciems',
      }
    },
    'EE': {
      name: 'Estonia',
      cities: {
        'Tallinn': 'Tallinn',
      }
    },
    'UK': {
      name: 'United Kingdom',
      cities: {
      }
    },
  }
};

export { api, maxFavorites, locations };

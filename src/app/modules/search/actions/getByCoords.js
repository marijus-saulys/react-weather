import axios from 'axios';

import { api } from '../../../config';
import handleError from './errorHandler';

const action = (lat, lon) => async () => {
  try {
    const { data } = await axios({
      url: `${api.weather.API_ROOT}?lat=${lat}&lon=${lon}&units=metric&APPID=${api.weather.API_KEY}`,
      method: 'GET',
    });
    return data;
  } catch (error) {
    throw new Error(handleError(error));
  }
};

export default action;
